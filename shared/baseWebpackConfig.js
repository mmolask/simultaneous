import path from "path";
import webpack from "webpack";

export const src_root = path.join(__dirname, "src", "js");
export const less_root = path.join(__dirname, "src", "less");
export const containers_src = path.join(src_root, "containers");

export default {
    devtool: 'eval',
    output: {
        sourceMapFilename: "[name].sourcemap.json",
        path: require("path").resolve("./lib"),
        filename: "[name].js",
        publicPath: 'http://localhost:3001/static/'
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.DefinePlugin({
            "process.env": {
                "NODE_ENV": JSON.stringify("development")
            }
        })
    ],
    resolve: {
        alias: {
            "dev/DevTools.js": path.join(containers_src, "DevTools.js"),
            "stores/configureStore.js": path.join(src_root, "stores", "configureStore.dev.js")
        }
    },
    resolveLoader: {
        'fallback': path.join(__dirname, 'node_modules')
    },
    module: {
        loaders: [{
            test: /\.js$/,
            loaders: ['react-hot', 'babel'],
            exclude: /node_modules/,
            include: src_root
        }, {
            test: /\.less$/,
            exclude: /node_modules/,
            include: less_root,
            loader: 'style!css!less'
        }]
    }
};