import { createAction, handleAction, handleActions } from 'redux-actions';
// TODO map this client dependency to a client/server injected thingy
import { fetchInitData } from "../client";
import keyMirror from "key-mirror";

export const ACTIONS = keyMirror({
    INIT_APP: null
});

export const startApp = createAction(ACTIONS.INIT_APP, fetchInitData);