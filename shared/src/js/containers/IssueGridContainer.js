import React from "react";
import { connect } from 'react-redux'
import CommentList from "../components/index.js"

const mapStateToProps = state => ({
    issues: state.issues,
    baseUrl: state.baseUrl
});

const mapDispatchToProps = (dispatch, props) => ({
});

const CommentListContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(CommentList);

export default CommentListContainer